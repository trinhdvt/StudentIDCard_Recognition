FROM python:3.9-slim

RUN apt-get update && apt-get install -y python3-pip python-dev build-essential

RUN python3 -m pip install --upgrade pip setuptools wheel

WORKDIR /app

COPY . /app

RUN pip3 install -r requirements.txt --no-cache-dir

RUN pip3 install torch==1.9.1+cpu torchvision==0.10.1+cpu torchaudio==0.9.1 -f https://download.pytorch.org/whl/torch_stable.html --no-cache-dir

RUN pip3 install gunicorn

CMD ["gunicorn", "--bind","0.0.0.0:80", "server:app"]

